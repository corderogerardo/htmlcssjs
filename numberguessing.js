/*Global variables*/
var finished = false;
var guess_input;
var guess_input_text;
var guesses = 0;
var target;
function do_game(){
	/*Local variables*/
	var ramdom_number = Math.random() * 100;
	var ramdom_number_integer = Math.floor(ramdom_number);
	target = ramdom_number_integer + 1;
	while(!finished){
		guess_input_text = prompt("I am thinking of a number  in the range 1 to 100. \n\n what is the number?");
	
	
	guess_input = parseInt(guess_input_text);
	guesses +=1;
	finished = check_guess();
	}/*End while*/
}/*End function*/

function check_guess(){
	if(isNaN(guess_input)){
		alert("You have not entered a number \n\n Please enter a number in the range 1 to 100");
		return false;
	}
	if((guess_input<1) || (guess_input)>100){
		alert("Please enter an integer number in the range 1 to 100");
		return false;
	}
	if(guess_input > target){
		alert("Your number is too large!");
		return false;
	}
	if(guess_input <target){
		alert("Your number is too small!");
		return false;
	}
	alert("You got it The number was "+target + ".\n\n It took you "+guesses+" guesses to get the number!");
	return true;
}