/*Part 1*/
/*Global variables*/
var finished = false;
var guess_input;
var guess_input_text;
var guesses = 0;
var target;
var arrColors = ["orange","red","green","yellow","blue","purple","gray","black","violet","pink","chocolate","brown","tomato"];
arrColors.sort();
function do_game(){
	/*Local variables*/
	var ramdom_number = Math.random() * arrColors.length;
	var ramdom_number_integer = Math.floor(ramdom_number);
	target = ramdom_number_integer ;
	alert("The correct answer is "+arrColors[target]);
	while(!finished){
		guess_input_text = prompt("I am thinking in one of these colors black blue brown chocolate gray green orange pink purple red tomato violet yellow \n\n what color am I thinking of?");
	guess_input = arrColors.indexOf(guess_input_text);
	guesses +=1;
	if(guess_input==target){
		document.body.style.background = arrColors[target];
	alert("Congratulations! You have guessed the color "+arrColors[target] + ".\n\n It took you "+guesses+" guesses to finish the game!\n\nYou can see the colour in the background.");
	return finished=true;
	}
	
	}/*End while*/
}/*End function*/

/*Part 2*/
/*Global variables*/
var finished = false;
var guess_input;
var guess_input_text;
var guesses = 0;
var target;
var arrColors = ["orange","red","green","yellow","blue","purple","gray","black","violet","pink","chocolate","brown","tomato"];
arrColors.sort();
function do_game(){
	/*Local variables*/
	var ramdom_number = Math.random() * arrColors.length;
	var ramdom_number_integer = Math.floor(ramdom_number);
	target = ramdom_number_integer ;
	alert("The correct answer is "+arrColors[target]);
	while(!finished){
		guess_input_text = prompt("I am thinking in one of these colors black blue brown chocolate gray green orange pink purple red tomato violet yellow \n\n what color am I thinking of?");
	guess_input = arrColors.indexOf(guess_input_text);
	guesses +=1;
	finished = check_guess();
	}/*End while*/
}/*End function*/
function check_guess(){
	if((guess_input<0) ){
		alert("Sorry, I don't recognize your color. \n\n Please try again");
		return false;
	}
	if(guess_input > target){
		alert("Sorry, your guess is not correct!\n\n Your color is alphabetically higher than the mine!. \n\n");
		return false;
	}
	if(guess_input < target){
		alert("Sorry, your guess is not correct!\n\n Your color is alphabetically lower  than the mine!. \n\n");
		return false;
	}
	document.body.style.background = arrColors[target];
	alert("Congratulations! You have guessed the color "+arrColors[target] + ".\n\n It took you "+guesses+" guesses to finish the game!\n\nYou can see the colour in the background.");
	return true;
}